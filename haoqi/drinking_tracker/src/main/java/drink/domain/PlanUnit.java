package drink.domain;

import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PlanUnit {

    @NotNull
    @Min(0L)
    private int amount;

    @NotNull
    @Min(1L)
    @Max(366L)
    private short dayIndex;

    @NotNull
    @ManyToOne
    private Plan plan;

    @NotNull
    @ManyToOne
    private Beverage beverage;
}
