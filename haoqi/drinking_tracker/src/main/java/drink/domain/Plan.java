package drink.domain;

import java.sql.Timestamp;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPlansByDrinker" })
public class Plan {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Timestamp startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Timestamp endDate;

    @NotNull
    @ManyToOne
    private Drinker drinker;
}
