package drink.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Beverage {

    @NotNull
    private String name;

    @NotNull
    @DecimalMin("0.00")
    @DecimalMax("100.00")
    @Digits(integer = 2, fraction = 2)
    private BigDecimal volume;

    @ManyToOne
    private Drinker drinker;
    
    public static Beverage findBeverageByDrinkerAndId(Long id, Drinker drinker) {
        if (id == null || drinker == null) return null;
        EntityManager em = Beverage.entityManager();
        TypedQuery<Beverage> q = em.createQuery("SELECT o FROM Beverage AS o WHERE o.drinker = :drinker and o.id = :id", Beverage.class);
        q.setParameter("drinker", drinker);
        q.setParameter("id", id);
        return q.getSingleResult();
    }
    
    public static List<Beverage> findBeverageEntriesByDrinker(int firstResult, int maxResults, Drinker drinker) {
    	EntityManager em = Beverage.entityManager();
        TypedQuery<Beverage> q = em.createQuery("SELECT o FROM Beverage AS o WHERE o.drinker = :drinker", Beverage.class);
        q.setParameter("drinker", drinker);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
    	return q.getResultList();
    }
    
    public static List<Beverage> findAllBeveragesByDrinker(Drinker drinker) {
    	EntityManager em = Beverage.entityManager();
        TypedQuery<Beverage> q = em.createQuery("SELECT o FROM Beverage AS o WHERE o.drinker = :drinker", Beverage.class);
        q.setParameter("drinker", drinker);
    	return q.getResultList();
    }
    
    public static long countBeveragesByDrinker(Drinker drinker) {
    	EntityManager em = Beverage.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Beverage o WHERE o.drinker = :drinker", Long.class);
        q.setParameter("drinker", drinker);
        return q.getSingleResult();
    }

	public static List<Beverage> findAllBeverages() {
        return entityManager().createQuery("SELECT o FROM Beverage o", Beverage.class).getResultList();
    }
}
