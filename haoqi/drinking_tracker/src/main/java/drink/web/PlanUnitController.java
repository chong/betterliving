package drink.web;

import drink.domain.PlanUnit;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/planunits")
@Controller
@RooWebScaffold(path = "planunits", formBackingObject = PlanUnit.class)
public class PlanUnitController {
}
