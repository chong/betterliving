package drink.web;

import drink.domain.Beverage;
import drink.domain.Drinker;
import drink.domain.Log;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/logs")
@Controller
@RooWebScaffold(path = "logs", formBackingObject = Log.class)
public class LogController {

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
        String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		populateEditForm(uiModel, new Log(), drinker);
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Beverage.countBeveragesByDrinker(drinker) == 0) {
            dependencies.add(new String[] { "beverage", "beverages" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "logs/create";
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Log log, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
		String username = principal.getName();
		if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, log, Drinker.findDrinkerByUsernameEquals(username));
            return "logs/create";
        }
		log.setDrinker(Drinker.findDrinkerByUsernameEquals(username));
        uiModel.asMap().clear();
        log.persist();
        return "redirect:/logs/" + encodeUrlPathSegment(log.getId().toString(), httpServletRequest);
    }

	void populateEditForm(Model uiModel, Log log, Drinker drinker) {
        uiModel.addAttribute("log", log);
        addDateTimeFormatPatterns(uiModel);
		if(drinker != null)
			uiModel.addAttribute("beverages", Beverage.findAllBeveragesByDrinker(drinker));
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Log log, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, log, drinker);
            return "logs/update";
        }
		log.setDrinker(drinker);
        uiModel.asMap().clear();
        log.merge();
        return "redirect:/logs/" + encodeUrlPathSegment(log.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
        populateEditForm(uiModel, Log.findLogByDrinkerAndId(id, drinker), drinker);
        return "logs/update";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("logs", Log.findLogEntriesByDrinker(firstResult, sizeNo, drinker));
            float nrOfPages = (float) Log.countLogsByDrinker(drinker) / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("logs", Log.findAllLogsByDrinker(drinker));
        }
        addDateTimeFormatPatterns(uiModel);
        return "logs/list";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel, Principal principal) {
		String username = principal.getName();
		Drinker drinker = Drinker.findDrinkerByUsernameEquals(username);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("log", Log.findLogByDrinkerAndId(id, drinker));
        uiModel.addAttribute("itemId", id);
        return "logs/show";
    }
}
