// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package drink.domain;

import drink.domain.Beverage;
import drink.domain.Plan;
import drink.domain.PlanUnit;

privileged aspect PlanUnit_Roo_JavaBean {
    
    public int PlanUnit.getAmount() {
        return this.amount;
    }
    
    public void PlanUnit.setAmount(int amount) {
        this.amount = amount;
    }
    
    public short PlanUnit.getDayIndex() {
        return this.dayIndex;
    }
    
    public void PlanUnit.setDayIndex(short dayIndex) {
        this.dayIndex = dayIndex;
    }
    
    public Plan PlanUnit.getPlan() {
        return this.plan;
    }
    
    public void PlanUnit.setPlan(Plan plan) {
        this.plan = plan;
    }
    
    public Beverage PlanUnit.getBeverage() {
        return this.beverage;
    }
    
    public void PlanUnit.setBeverage(Beverage beverage) {
        this.beverage = beverage;
    }
    
}
