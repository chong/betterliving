package com.betterliving.weightmanagement.doman;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Customer {

    @NotNull
    @Column(unique = true)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String username;

    @NotNull
    private String password;
    
    @PrePersist
    @PreUpdate
    protected void encryptPassword() {
        password = DigestUtils.sha256Hex(password);
    }
    
    public static Customer findCustomerByUsername(String Username) {
        if (Username == null) return null;
        EntityManager em = Customer.entityManager();
        TypedQuery<Customer> q = em.createQuery("SELECT o FROM Customer AS o WHERE o.username = :username", Customer.class);
        q.setParameter("username", Username);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getSingleResult();
    }
}
