// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.betterliving.weightmanagement.doman;

import com.betterliving.weightmanagement.doman.Comment;
import com.betterliving.weightmanagement.doman.Customer;
import java.math.BigInteger;
import java.util.Date;

privileged aspect Comment_Roo_JavaBean {
    
    public Customer Comment.getCustomerId() {
        return this.customerId;
    }
    
    public void Comment.setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }
    
    public BigInteger Comment.getFriendId() {
        return this.friendId;
    }
    
    public void Comment.setFriendId(BigInteger friendId) {
        this.friendId = friendId;
    }
    
    public String Comment.getComment() {
        return this.comment;
    }
    
    public void Comment.setComment(String comment) {
        this.comment = comment;
    }
    
    public Date Comment.getCommentdate() {
        return this.commentdate;
    }
    
    public void Comment.setCommentdate(Date commentdate) {
        this.commentdate = commentdate;
    }
    
}
