package com.betterliving.weightmanagement.doman;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Profile {

    @OneToOne
    @NotNull
    private Customer customerId;

    @NotNull
    private String surname;

    @NotNull
    private String othername;

    @NotNull
    private String email;

    @NotNull
    private int gender;

    @NotNull
    private Float height;

    @NotNull
    private Float originalweight;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Customer> friendList = new HashSet<Customer>();
    
    public static Profile findProfileByUsername(String Username) {
    	if (Username == null) return null;
    	Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = Profile.entityManager();
        TypedQuery<Profile> q = em.createQuery("SELECT o FROM Profile AS o WHERE o.customerId = :customerId", Profile.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getSingleResult();
    }

}
