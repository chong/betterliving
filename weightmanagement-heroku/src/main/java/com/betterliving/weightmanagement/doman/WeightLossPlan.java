package com.betterliving.weightmanagement.doman;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class WeightLossPlan {

    @ManyToOne
    @NotNull
    private Customer customerId;

    @NotNull
    private Float startweight;

    @NotNull
    private Float endweight;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date startdate;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date enddate;
    
    private String detail;
    
    public static WeightLossPlan findCurrentPlanByUsername(String Username) {
        if (Username == null)
        	return null;
        Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = WeightLossPlan.entityManager();
        TypedQuery<WeightLossPlan> q = em.createQuery("SELECT o FROM WeightLossPlan AS o WHERE o.customerId = :customerId order by id desc", WeightLossPlan.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getResultList().get(0);
    }
    
    public static List<WeightLossPlan> findAllPlanByUsername(String Username) {
        if (Username == null)
        	return null;
        Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = WeightLossPlan.entityManager();
        TypedQuery<WeightLossPlan> q = em.createQuery("SELECT o FROM WeightLossPlan AS o WHERE o.customerId = :customerId order by id desc", WeightLossPlan.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getResultList();
    }

}
