package com.betterliving.weightmanagement.doman;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class WeightStatus {

    @ManyToOne
    @NotNull
    private Customer customerId;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date logdate;

    @NotNull
    private Float weight;

    @NotNull
    private Float weightloss;

    private String diary;

    public static List<WeightStatus> findAllStatusByUsername(String Username) {
        if (Username == null)
        	return null;
        Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = WeightStatus.entityManager();
        TypedQuery<WeightStatus> q = em.createQuery("SELECT o FROM WeightStatus AS o WHERE o.customerId = :customerId order by id asc", WeightStatus.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getResultList();
    }
    
    public static WeightStatus findCurrentStatusByUsername(String Username) {
        if (Username == null)
        	return null;
        Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = WeightStatus.entityManager();
        TypedQuery<WeightStatus> q = em.createQuery("SELECT o FROM WeightStatus AS o WHERE o.customerId = :customerId order by id desc", WeightStatus.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getResultList().get(0);
    }
}
