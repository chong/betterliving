package com.betterliving.weightmanagement.doman;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Comment {

    @ManyToOne
    @NotNull
    private Customer customerId;
    
    @NotNull
    private BigInteger friendId;

    @NotNull
    private String comment;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date commentdate;
    
    public static List<Comment> findCommentsByUsername(String Username) {
    	if (Username == null) return null;
    	Customer customer = Customer.findCustomerByUsername(Username);
        EntityManager em = Comment.entityManager();
        TypedQuery<Comment> q = em.createQuery("SELECT o FROM Comment AS o WHERE o.customerId = :customerId", Comment.class);
        q.setParameter("customerId", customer);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getResultList();
    }

}
