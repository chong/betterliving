package com.betterliving.weightmanagement.web;

import com.betterliving.weightmanagement.doman.Comment;
import com.betterliving.weightmanagement.doman.Customer;
import com.betterliving.weightmanagement.doman.Profile;

import java.math.BigInteger;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/comments")
@Controller
@RooWebScaffold(path = "comments", formBackingObject = Comment.class)
public class CommentController {

	@RequestMapping(params = "addedCommentForCustomer", produces = "text/html")
    public String addedCommentForCustomer(@RequestParam(value = "addedCommentForCustomer", required = false) String Username, Model uiModel, Principal principal) {
		Customer myFriend = Customer.findCustomerByUsername(Username);
		Comment comment = new Comment();
        uiModel.addAttribute("comment", comment);
        addDateTimeFormatPatterns(uiModel);
        List<Customer> customer = new ArrayList<Customer> ();
        customer.add(myFriend);
        uiModel.addAttribute("customer", customer);
        List<BigInteger> friendId = new ArrayList<BigInteger> ();
        friendId.add(new BigInteger(Customer.findCustomerByUsername(principal.getName()).getId().toString()));
        uiModel.addAttribute("friendId", friendId);
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Customer.countCustomers() == 0) {
            dependencies.add(new String[] { "customer", "customers" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "comments/create";
    }
	
	@RequestMapping(params = "showComments", produces = "text/html")
    public String showComments(Model uiModel, Principal principal) {
        uiModel.addAttribute("comments", Comment.findCommentsByUsername(principal.getName()));
        addDateTimeFormatPatterns(uiModel);
        return "comments/list";
    }
}
