package com.betterliving.weightmanagement.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.betterliving.weightmanagement.doman.Customer;
import com.betterliving.weightmanagement.doman.Profile;

@RequestMapping("/profiles")
@Controller
@RooWebScaffold(path = "profiles", formBackingObject = Profile.class)
public class ProfileController {
	@RequestMapping(params = "showProfile", produces = "text/html")
    public String showProfile(Model uiModel, Principal principal) {
		uiModel.addAttribute("profile", Profile.findProfileByUsername(principal.getName()));
        return "profiles/show";
    }
	

	
    @RequestMapping(params = "updateProfileForm", produces = "text/html")
    public String updateProfileForm(Model uiModel, Principal principal) {
    	Profile profile = Profile.findProfileByUsername(principal.getName());
    	if (profile == null) 
    		return "redirect:/profiles?form";
        populateEditForm(uiModel, principal, profile);
        return "profiles/update";
    }
    

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Profile profile, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
		if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, profile);
            return "profiles/update";
        }
        uiModel.asMap().clear();
        profile.merge();
        return "redirect:/profiles/" + encodeUrlPathSegment(profile.getId().toString(), httpServletRequest);
    }
	


	void populateEditForm(Model uiModel, Principal principal, Profile profile) {
        uiModel.addAttribute("profile", profile);
        List<Customer> customer = new ArrayList<Customer> ();
        if(profile.getCustomerId() == null)
        	customer.add(Customer.findCustomerByUsername(principal.getName()));
        else
        	customer.add(profile.getCustomerId());
        uiModel.addAttribute("customer", customer);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, new Profile());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Customer.countCustomers() == 0) {
            dependencies.add(new String[] { "customer", "customers" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "profiles/create";
    }

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Profile profile, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, profile);
            return "profiles/create";
        }
        uiModel.asMap().clear();
        profile.persist();
        return "redirect:/profiles/" + encodeUrlPathSegment(profile.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, Profile.findProfile(id));
        return "profiles/update";
    }
}
