package com.betterliving.weightmanagement.web;

import java.security.Principal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.betterliving.weightmanagement.doman.Customer;
import com.betterliving.weightmanagement.doman.Profile;
import com.betterliving.weightmanagement.doman.WeightLossPlan;
import com.betterliving.weightmanagement.doman.WeightStatus;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/weightstatuses")
@Controller
@RooWebScaffold(path = "weightstatuses", formBackingObject = WeightStatus.class)
public class WeightStatusController {

	@RequestMapping(params = "showAllStatus", produces = "text/html")
    public String showAllStatus(Model uiModel, Principal principal) {
		addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("weightstatuses", WeightStatus.findAllStatusByUsername(principal.getName()));
        return "weightstatuses/list";
    }
	

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid WeightStatus weightStatus, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, weightStatus);
            return "weightstatuses/create";
        }
        uiModel.asMap().clear();
        weightStatus.persist();
        return "redirect:/weightstatuses/" + encodeUrlPathSegment(weightStatus.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, new WeightStatus());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Customer.countCustomers() == 0) {
            dependencies.add(new String[] { "customer", "customers" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "weightstatuses/create";
    }

	void populateEditForm(Model uiModel, Principal principal, WeightStatus weightStatus) {
        uiModel.addAttribute("weightStatus", weightStatus);
        addDateTimeFormatPatterns(uiModel);
        List<Customer> customer = new ArrayList<Customer> ();
        if(weightStatus.getCustomerId() == null)
        	customer.add(Customer.findCustomerByUsername(principal.getName()));
        else
        	customer.add(weightStatus.getCustomerId());
        uiModel.addAttribute("customer", customer);
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid WeightStatus weightStatus, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, weightStatus);
            return "weightstatuses/update";
        }
        uiModel.asMap().clear();
        weightStatus.merge();
        return "redirect:/weightstatuses/" + encodeUrlPathSegment(weightStatus.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, WeightStatus.findWeightStatus(id));
        return "weightstatuses/update";
    }
	
	@RequestMapping(params = "showWeightChart", produces = "text/html")
    public String showWeightChart() {
        return "weightstatuses/showWeightChart";
	}
	
	@RequestMapping(params = "getWeightChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getWeightChartData(Principal principal) {
		List<WeightStatus> weightstatuses = WeightStatus.findAllStatusByUsername(principal.getName());
		
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(1);
		
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Label");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Value");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		Iterator it = weightstatuses.iterator();
		int num = 0;
		while(it.hasNext())
		{
			num++;
			WeightStatus weightstatus = (WeightStatus) it.next();
			ModelMap row = new ModelMap();
			List<ModelMap> cells = new ArrayList<ModelMap>();
			ModelMap cell = new ModelMap();
			cell.addAttribute("v", "Weight Status "+num);
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", Float.parseFloat(format.format(weightstatus.getWeight())));
			cells.add((ModelMap) cell.clone());
			row.addAttribute("c", cells);
			rows.add(row);
		}
		data.addAttribute("rows", rows);
		return data;
    }
	
	@RequestMapping(params = "showBMIChart", produces = "text/html")
    public String showBMIChart() {
        return "weightstatuses/showBMIChart";
	}
	
	@RequestMapping(params = "getBMIChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getBMIChartData(Principal principal) {
		List<WeightStatus> weightstatuses = WeightStatus.findAllStatusByUsername(principal.getName());
		Profile profile = Profile.findProfileByUsername(principal.getName());
		
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(1);
		
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Label");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Value");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		Iterator it = weightstatuses.iterator();
		int num = 0;
		while(it.hasNext())
		{
			num++;
			WeightStatus weightstatus = (WeightStatus) it.next();
			ModelMap row = new ModelMap();
			List<ModelMap> cells = new ArrayList<ModelMap>();
			ModelMap cell = new ModelMap();
			cell.addAttribute("v", "Weight Status "+num);
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", Float.parseFloat(format.format(weightstatus.getWeight()/((profile.getHeight()/100)*(profile.getHeight()/100)))));
			cells.add((ModelMap) cell.clone());
			row.addAttribute("c", cells);
			rows.add(row);
		}
		data.addAttribute("rows", rows);
		return data;
    }
	
	@RequestMapping(params = "showWeightLossChart", produces = "text/html")
    public String showWeightLossChart() {
        return "weightstatuses/showWeightLossChart";
	}
	
	@RequestMapping(params = "getWeightLossChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getWeightLossChartData(Principal principal) {
		List<WeightStatus> weightstatuses = WeightStatus.findAllStatusByUsername(principal.getName());
		
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(1);
		
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Label");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Value");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		Iterator it = weightstatuses.iterator();
		int num = 0;
		while(it.hasNext())
		{
			num++;
			WeightStatus weightstatus = (WeightStatus) it.next();
			ModelMap row = new ModelMap();
			List<ModelMap> cells = new ArrayList<ModelMap>();
			ModelMap cell = new ModelMap();
			cell.addAttribute("v", "Weight Loss Status "+num);
			cells.add((ModelMap) cell.clone());
			cell.addAttribute("v", Float.parseFloat(format.format(weightstatus.getWeightloss())));
			cells.add((ModelMap) cell.clone());
			row.addAttribute("c", cells);
			rows.add(row);
		}
		data.addAttribute("rows", rows);
		return data;
    }
}
