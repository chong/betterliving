package com.betterliving.weightmanagement.web;

import java.math.BigInteger;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.betterliving.weightmanagement.doman.Comment;
import com.betterliving.weightmanagement.doman.Customer;
import com.betterliving.weightmanagement.doman.Profile;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/customers")
@Controller
@RooWebScaffold(path = "customers", formBackingObject = Customer.class)
public class CustomerController {
	@RequestMapping(params = "searchFriend", produces = "text/html")
    public String searchFriend(Principal principal) {
        return "friendses/search";
    }
	
	@RequestMapping(params = "searchByUsername", produces = "text/html")
    public String searchByUsername(@RequestParam(value = "searchByUsername", required = false) String Username, Model uiModel, Principal principal) {
		Customer friend = Customer.findCustomerByUsername(Username);
		if(friend != null)
		{
			uiModel.addAttribute("friend", friend.getUsername());
			if(friend.getUsername().equals(principal.getName()))
				uiModel.addAttribute("isSelf", true);
			else
				uiModel.addAttribute("isSelf", false);
		}
		else
			uiModel.addAttribute("friend", null);
		return "friendses/showSearchedFriend";
    }
	
	@RequestMapping(params = "addFriendByUsername", produces = "text/html")
    public String addFriendByUsername(@RequestParam(value = "addFriendByUsername", required = false) String Username, Model uiModel, Principal principal) {
		Customer friend = Customer.findCustomerByUsername(Username);
		Profile myProfile = Profile.findProfileByUsername(principal.getName());
		myProfile.getFriendList().add(friend);
		myProfile.merge();
		uiModel.addAttribute("friend", friend.getUsername());
		return "friendses/addFriend";
    }
	
	@RequestMapping(params = "showFriends", produces = "text/html")
    public String showFriends(Model uiModel, Principal principal) {
		Profile profile = Profile.findProfileByUsername(principal.getName());
		List<String> friendList = new ArrayList<String>();
		Iterator it = profile.getFriendList().iterator();
		while(it.hasNext())
		{
			Customer friend = (Customer) it.next();
			friendList.add(friend.getUsername());
		}
		uiModel.addAttribute("friends", friendList);
        return "friendses/showFriends";
    }
	
	@RequestMapping(params = "removeFriendByUsername", produces = "text/html")
    public String removeFriendByUsername(@RequestParam(value = "removeFriendByUsername", required = false) String Username, Model uiModel, Principal principal) {
		Customer myFriend = Customer.findCustomerByUsername(Username);
		Profile myProfile = Profile.findProfileByUsername(principal.getName());
		myProfile.getFriendList().remove(myFriend);
		myProfile.merge();
		List<String> friendList = new ArrayList<String>();
		Iterator it = myProfile.getFriendList().iterator();
		while(it.hasNext())
		{
			Customer friend = (Customer) it.next();
			friendList.add(friend.getUsername());
		}
		uiModel.addAttribute("friends", friendList);
        return "friendses/showFriends";
    }
}
