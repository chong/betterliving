package com.betterliving.weightmanagement.web;

import java.security.Principal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.betterliving.weightmanagement.doman.Customer;
import com.betterliving.weightmanagement.doman.Profile;
import com.betterliving.weightmanagement.doman.WeightLossPlan;
import com.betterliving.weightmanagement.doman.WeightStatus;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/weightlossplans")
@Controller
@RooWebScaffold(path = "weightlossplans", formBackingObject = WeightLossPlan.class)
public class WeightLossPlanController {
	@RequestMapping(params = "showCurrentPlan", produces = "text/html")
    public String showCurrentPlan(Model uiModel, Principal principal) {
		addDateTimeFormatPatterns(uiModel);
		WeightLossPlan weightlossplan = WeightLossPlan.findCurrentPlanByUsername(principal.getName());
		uiModel.addAttribute("weightlossplan", weightlossplan);
        return "weightlossplans/show";
    }
	
	@RequestMapping(params = "showAllPlan", produces = "text/html")
    public String showAllPlan(Model uiModel, Principal principal) {
		addDateTimeFormatPatterns(uiModel);
		List<WeightLossPlan> weightlossplans = WeightLossPlan.findAllPlanByUsername(principal.getName());
		uiModel.addAttribute("weightlossplans", weightlossplans);
        return "weightlossplans/list";
    }
	
	@RequestMapping(params = "showChart", produces = "text/html")
    public String showChart() {
        return "weightlossplans/showChart";
    }
	
	@RequestMapping(params = "getChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getChartData(Principal principal) {
		WeightLossPlan weightlossplan = WeightLossPlan.findCurrentPlanByUsername(principal.getName());
		WeightStatus weightstatus = WeightStatus.findCurrentStatusByUsername(principal.getName());
		float doc = (weightlossplan.getStartweight() - weightstatus.getWeight()) / (weightlossplan.getStartweight() - weightlossplan.getEndweight());
		doc = doc * 100;
		if(doc < 0)
			doc = 0;
		if(doc > 100)
			doc = 100;
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Label");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Value");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		ModelMap row = new ModelMap();
		List<ModelMap> cells = new ArrayList<ModelMap>();
		ModelMap cell = new ModelMap();
		cell.addAttribute("v", "Weight Loss(%)");
		cells.add((ModelMap) cell.clone());
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(1);
		cell.addAttribute("v", Float.parseFloat(format.format(doc)));
		cells.add((ModelMap) cell.clone());
		row.addAttribute("c", cells);
		rows.add(row);
		data.addAttribute("rows", rows);
		return data;
    }


	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid WeightLossPlan weightLossPlan, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, weightLossPlan);
            return "weightlossplans/create";
        }
        uiModel.asMap().clear();
        weightLossPlan.persist();
        return "redirect:/weightlossplans/" + encodeUrlPathSegment(weightLossPlan.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, new WeightLossPlan());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Customer.countCustomers() == 0) {
            dependencies.add(new String[] { "customer", "customers" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "weightlossplans/create";
    }

	void populateEditForm(Model uiModel, Principal principal, WeightLossPlan weightLossPlan) {
        uiModel.addAttribute("weightLossPlan", weightLossPlan);
        addDateTimeFormatPatterns(uiModel);
        List<Customer> customer = new ArrayList<Customer> ();
        if(weightLossPlan.getCustomerId() == null)
        	customer.add(Customer.findCustomerByUsername(principal.getName()));
        else
        	customer.add(weightLossPlan.getCustomerId());
        uiModel.addAttribute("customer", customer);
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid WeightLossPlan weightLossPlan, BindingResult bindingResult, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, principal, weightLossPlan);
            return "weightlossplans/update";
        }
        uiModel.asMap().clear();
        weightLossPlan.merge();
        return "redirect:/weightlossplans/" + encodeUrlPathSegment(weightLossPlan.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
        populateEditForm(uiModel, principal, WeightLossPlan.findWeightLossPlan(id));
        return "weightlossplans/update";
    }
}
