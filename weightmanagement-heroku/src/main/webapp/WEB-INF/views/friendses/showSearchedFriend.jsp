<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div>
<c:choose>
	<c:when test="${not empty friend}">
		<c:choose>
			<c:when test="${isSelf == false}">
				<form>
				Find User:
				<table>
				<tr>
				<td>
				${friend}
				</td>
				<td>
				<input type="hidden" name="addFriendByUsername" value="${friend}">
				<input type="submit" value="Add Friend">
				</td>
				</tr>
				</table>
				</form>
			</c:when>
			<c:otherwise>
			This is just you!
			<a href="/customers?searchFriend"><button type="button">Back</button></a>
			</c:otherwise>
		</c:choose>
      </c:when>
      <c:otherwise>
      No User find.
      <a href="/customers?searchFriend"><button type="button">Back</button></a>
      </c:otherwise>
</c:choose>
</div>