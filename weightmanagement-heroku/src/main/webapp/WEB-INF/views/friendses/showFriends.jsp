<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div>
<c:choose>
	<c:when test="${not empty friends}">
	My Friends:
	<table>
	<c:forEach var="friend" items="${friends}">
		<tr>
			<td>${friend}</td>
			<td>
				<a href="/comments?addedCommentForCustomer=${friend}"><button type="button">Send message</button></a>
			</td>
			<td>
				<form>
				<input type="hidden" name="removeFriendByUsername" value="${friend}">
				<input type="submit" value="Remove">
				</form>
			</td>
		</tr>
	</c:forEach>
	</table>
	</c:when>
	<c:otherwise>
	You have no friends yet!
	<a href="/customers?searchFriend"><button type="button">Search For Friends</button></a>
	</c:otherwise>
</c:choose>
</div>