<div xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:fn="http://java.sun.com/jsp/jstl/functions" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags" version="2.0">
    <h3>Degree Of Your Weight Loss Plan Completion:</h3>
    <br/>
	<script type='text/javascript'>
		// Load the Visualization API and the piechart package.
    	google.load('visualization', '1', {'packages':['gauge']});
      
    	// Set a callback to run when the Google Visualization API is loaded.
    	google.setOnLoadCallback(drawChart);
    	
    	function drawChart() {
    	    var jsonData = $.ajax({
    	    	type:'GET',
    	        url: "/weightlossplans?getChartData",
    	        dataType:"json",
    	        async: false
    	        }).responseText;   
    	    // Create our data table out of JSON data loaded from server.
    	    var data = new google.visualization.DataTable(jsonData);
    	    
            var options = {
                    width: 400, height: 400,
                    redFrom: 90, redTo: 100,
                    yellowFrom:75, yellowTo: 90,
                    minorTicks: 5
                  };
            
            var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
            chart.draw(data, options);
    	}
	</script>
	<div id="chart_div" style="width: 650px; height: 400px;">loading the chart...</div>
</div>