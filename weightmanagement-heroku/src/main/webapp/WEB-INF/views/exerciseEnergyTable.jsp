<div>
<h3>Exercise Energy Charts - Calories:</h3>
<table width="100%" align="center">

                                          
<tbody><tr height="35">

                                                <td width="255" height="35" class="bodyText"><strong>Activity </strong></td>

                                                <td width="78" class="bodyText"><div align="center"><strong>Cal/Kg </strong></div></td>

                                                <td width="64" class="bodyText"><div align="center"><strong>50Kg </strong></div></td>

                                                <td width="64" class="bodyText"><div align="center"><strong>100Kg </strong></div></td>

                                                <td width="64" class="bodyText"><div align="center"><strong>150Kg </strong></div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Aerobics

                                                  (low impact) </td>

                                                <td class="bodyText" width="78"><div align="center">2.54 </div></td>

                                                <td class="bodyText"><div align="center">127 </div></td>

                                                <td class="bodyText"><div align="center">254 </div></td>

                                                <td class="bodyText"><div align="center">380 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Step Aerobics

                                                  (beginner) </td>

                                                <td class="bodyText" width="78"><div align="center">3.20 </div></td>

                                                <td class="bodyText"><div align="center">160 </div></td>

                                                <td class="bodyText"><div align="center">320 </div></td>

                                                <td class="bodyText"><div align="center">479 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Badminton </td>

                                                <td class="bodyText" width="78"><div align="center">3.31 </div></td>

                                                <td class="bodyText"><div align="center">165 </div></td>

                                                <td class="bodyText"><div align="center">331 </div></td>

                                                <td class="bodyText"><div align="center">496 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Basketball

                                                  (game) </td>

                                                <td class="bodyText" width="78"><div align="center">4.85 </div></td>

                                                <td class="bodyText"><div align="center">243 </div></td>

                                                <td class="bodyText"><div align="center">485 </div></td>

                                                <td class="bodyText"><div align="center">728 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Basketball

                                                  (leisurely) </td>

                                                <td class="bodyText" width="78"><div align="center">2.87 </div></td>

                                                <td class="bodyText"><div align="center">143 </div></td>

                                                <td class="bodyText"><div align="center">287 </div></td>

                                                <td class="bodyText"><div align="center">430 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Bicycling,

                                                  15kph </td>

                                                <td class="bodyText" width="78"><div align="center">2.76 </div></td>

                                                <td class="bodyText"><div align="center">138 </div></td>

                                                <td class="bodyText"><div align="center">276 </div></td>

                                                <td class="bodyText"><div align="center">413 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Bicycling,

                                                  20kph </td>

                                                <td class="bodyText" width="78"><div align="center">4.41 </div></td>

                                                <td class="bodyText"><div align="center">220 </div></td>

                                                <td class="bodyText"><div align="center">441 </div></td>

                                                <td class="bodyText"><div align="center">661 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Bowling </td>

                                                <td class="bodyText" width="78"><div align="center">1.21 </div></td>

                                                <td class="bodyText"><div align="center">61 </div></td>

                                                <td class="bodyText"><div align="center">121 </div></td>

                                                <td class="bodyText"><div align="center">182 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Canoeing,

                                                  4kph </td>

                                                <td class="bodyText" width="78"><div align="center">1.54 </div></td>

                                                <td class="bodyText"><div align="center">77 </div></td>

                                                <td class="bodyText"><div align="center">154 </div></td>

                                                <td class="bodyText"><div align="center">231 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Canoeing,

                                                  6.5kph </td>

                                                <td class="bodyText" width="78"><div align="center">2.98 </div></td>

                                                <td class="bodyText"><div align="center">149 </div></td>

                                                <td class="bodyText"><div align="center">298 </div></td>

                                                <td class="bodyText"><div align="center">446 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Cross

                                                  country ski (hard) </td>

                                                <td class="bodyText" width="78"><div align="center">7.28 </div></td>

                                                <td class="bodyText"><div align="center">364 </div></td>

                                                <td class="bodyText"><div align="center">728 </div></td>

                                                <td class="bodyText"><div align="center">1091 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Cross

                                                  country ski (easy) </td>

                                                <td class="bodyText" width="78"><div align="center">3.42 </div></td>

                                                <td class="bodyText"><div align="center">171 </div></td>

                                                <td class="bodyText"><div align="center">342 </div></td>

                                                <td class="bodyText"><div align="center">513 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Cross

                                                  country ski (mod) </td>

                                                <td class="bodyText" width="78"><div align="center">4.85 </div></td>

                                                <td class="bodyText"><div align="center">243 </div></td>

                                                <td class="bodyText"><div align="center">485 </div></td>

                                                <td class="bodyText"><div align="center">728 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Dancing </td>

                                                <td class="bodyText" width="78"><div align="center">2.20 </div></td>

                                                <td class="bodyText"><div align="center">110 </div></td>

                                                <td class="bodyText"><div align="center">220 </div></td>

                                                <td class="bodyText"><div align="center">331 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Dancing

                                                  (slow) </td>

                                                <td class="bodyText" width="78"><div align="center">1.21 </div></td>

                                                <td class="bodyText"><div align="center">61 </div></td>

                                                <td class="bodyText"><div align="center">121 </div></td>

                                                <td class="bodyText"><div align="center">182 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Golfing

                                                  (walk w/o cart) </td>

                                                <td class="bodyText" width="78"><div align="center">2.20 </div></td>

                                                <td class="bodyText"><div align="center">110 </div></td>

                                                <td class="bodyText"><div align="center">220 </div></td>

                                                <td class="bodyText"><div align="center">331 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Golfing

                                                  (with a cart) </td>

                                                <td class="bodyText" width="78"><div align="center">1.54 </div></td>

                                                <td class="bodyText"><div align="center">77 </div></td>

                                                <td class="bodyText"><div align="center">154 </div></td>

                                                <td class="bodyText"><div align="center">231 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Handball </td>

                                                <td class="bodyText" width="78"><div align="center">5.07 </div></td>

                                                <td class="bodyText"><div align="center">254 </div></td>

                                                <td class="bodyText"><div align="center">507 </div></td>

                                                <td class="bodyText"><div align="center">761 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Hiking

                                                  with  5kg load </td>

                                                <td class="bodyText" width="78"><div align="center">3.97 </div></td>

                                                <td class="bodyText"><div align="center">198 </div></td>

                                                <td class="bodyText"><div align="center">397 </div></td>

                                                <td class="bodyText"><div align="center">595 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Hiking

                                                  with 10kg load </td>

                                                <td class="bodyText" width="78"><div align="center">4.41 </div></td>

                                                <td class="bodyText"><div align="center">220 </div></td>

                                                <td class="bodyText"><div align="center">441 </div></td>

                                                <td class="bodyText"><div align="center">661 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Hiking

                                                  with  15kg load </td>

                                                <td class="bodyText" width="78"><div align="center">5.18 </div></td>

                                                <td class="bodyText"><div align="center">259 </div></td>

                                                <td class="bodyText"><div align="center">518 </div></td>

                                                <td class="bodyText"><div align="center">777 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Hiking,

                                                  no load </td>

                                                <td class="bodyText" width="78"><div align="center">3.42 </div></td>

                                                <td class="bodyText"><div align="center">171 </div></td>

                                                <td class="bodyText"><div align="center">342 </div></td>

                                                <td class="bodyText"><div align="center">513 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Jogging,

                                                  8kph </td>

                                                <td class="bodyText" width="78"><div align="center">4.08 </div></td>

                                                <td class="bodyText"><div align="center">204 </div></td>

                                                <td class="bodyText"><div align="center">408 </div></td>

                                                <td class="bodyText"><div align="center">612 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Jogging,

                                                  10kph </td>

                                                <td class="bodyText" width="78"><div align="center">5.07 </div></td>

                                                <td class="bodyText"><div align="center">254 </div></td>

                                                <td class="bodyText"><div align="center">507 </div></td>

                                                <td class="bodyText"><div align="center">761 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Raquetball </td>

                                                <td class="bodyText" width="78"><div align="center">4.52 </div></td>

                                                <td class="bodyText"><div align="center">226 </div></td>

                                                <td class="bodyText"><div align="center">452 </div></td>

                                                <td class="bodyText"><div align="center">678 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Rowing

                                                  (leisurely) </td>

                                                <td class="bodyText" width="78"><div align="center">1.65 </div></td>

                                                <td class="bodyText"><div align="center">83 </div></td>

                                                <td class="bodyText"><div align="center">165 </div></td>

                                                <td class="bodyText"><div align="center">248 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Rowing

                                                  machine </td>

                                                <td class="bodyText" width="78"><div align="center">3.97 </div></td>

                                                <td class="bodyText"><div align="center">198 </div></td>

                                                <td class="bodyText"><div align="center">397 </div></td>

                                                <td class="bodyText"><div align="center">595 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Running,

                                                  13kph </td>

                                                <td class="bodyText" width="78"><div align="center">6.72 </div></td>

                                                <td class="bodyText"><div align="center">336 </div></td>

                                                <td class="bodyText"><div align="center">672 </div></td>

                                                <td class="bodyText"><div align="center">1009 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Running,

                                                  14kph </td>

                                                <td class="bodyText" width="78"><div align="center">7.28 </div></td>

                                                <td class="bodyText"><div align="center">364 </div></td>

                                                <td class="bodyText"><div align="center">728 </div></td>

                                                <td class="bodyText"><div align="center">1091 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Running,

                                                  16kph </td>

                                                <td class="bodyText" width="78"><div align="center">7.72 </div></td>

                                                <td class="bodyText"><div align="center">386 </div></td>

                                                <td class="bodyText"><div align="center">772 </div></td>

                                                <td class="bodyText"><div align="center">1157 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Skipping

                                                  rope </td>

                                                <td class="bodyText" width="78"><div align="center">6.28 </div></td>

                                                <td class="bodyText"><div align="center">314 </div></td>

                                                <td class="bodyText"><div align="center">628 </div></td>

                                                <td class="bodyText"><div align="center">942 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Snow

                                                  skiing, downhill </td>

                                                <td class="bodyText" width="78"><div align="center">2.87 </div></td>

                                                <td class="bodyText"><div align="center">143 </div></td>

                                                <td class="bodyText"><div align="center">287 </div></td>

                                                <td class="bodyText"><div align="center">430 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Soccer </td>

                                                <td class="bodyText" width="78"><div align="center">4.30 </div></td>

                                                <td class="bodyText"><div align="center">215 </div></td>

                                                <td class="bodyText"><div align="center">430 </div></td>

                                                <td class="bodyText"><div align="center">645 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Squash </td>

                                                <td class="bodyText" width="78"><div align="center">4.52 </div></td>

                                                <td class="bodyText"><div align="center">226 </div></td>

                                                <td class="bodyText"><div align="center">452 </div></td>

                                                <td class="bodyText"><div align="center">678 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Stair

                                                  climber machine </td>

                                                <td class="bodyText" width="78"><div align="center">3.53 </div></td>

                                                <td class="bodyText"><div align="center">176 </div></td>

                                                <td class="bodyText"><div align="center">353 </div></td>

                                                <td class="bodyText"><div align="center">529 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Stair

                                                  climbing </td>

                                                <td class="bodyText" width="78"><div align="center">3.09 </div></td>

                                                <td class="bodyText"><div align="center">154 </div></td>

                                                <td class="bodyText"><div align="center">309 </div></td>

                                                <td class="bodyText"><div align="center">463 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Swimming

                                                  (25 meters per minute) </td>

                                                <td class="bodyText" width="78"><div align="center">2.65 </div></td>

                                                <td class="bodyText"><div align="center">132 </div></td>

                                                <td class="bodyText"><div align="center">265 </div></td>

                                                <td class="bodyText"><div align="center">397 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Swimming

                                                  (50 meters per minute) </td>

                                                <td class="bodyText" width="78"><div align="center">4.96 </div></td>

                                                <td class="bodyText"><div align="center">248 </div></td>

                                                <td class="bodyText"><div align="center">496 </div></td>

                                                <td class="bodyText"><div align="center">744 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Tennis </td>

                                                <td class="bodyText" width="78"><div align="center">3.53 </div></td>

                                                <td class="bodyText"><div align="center">176 </div></td>

                                                <td class="bodyText"><div align="center">353 </div></td>

                                                <td class="bodyText"><div align="center">529 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Tennis

                                                  (doubles) </td>

                                                <td class="bodyText" width="78"><div align="center">2.43 </div></td>

                                                <td class="bodyText"><div align="center">121 </div></td>

                                                <td class="bodyText"><div align="center">243 </div></td>

                                                <td class="bodyText"><div align="center">364 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Volleyball

                                                  (game) </td>

                                                <td class="bodyText" width="78"><div align="center">2.65 </div></td>

                                                <td class="bodyText"><div align="center">132 </div></td>

                                                <td class="bodyText"><div align="center">265 </div></td>

                                                <td class="bodyText"><div align="center">397 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Volleyball

                                                  (leisurely) </td>

                                                <td class="bodyText" width="78"><div align="center">1.54 </div></td>

                                                <td class="bodyText"><div align="center">77 </div></td>

                                                <td class="bodyText"><div align="center">154 </div></td>

                                                <td class="bodyText"><div align="center">231 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Walking

                                                  3 kph (20 minutes per km) </td>

                                                <td class="bodyText" width="78"><div align="center">1.32 </div></td>

                                                <td class="bodyText"><div align="center">66 </div></td>

                                                <td class="bodyText"><div align="center">132 </div></td>

                                                <td class="bodyText"><div align="center">198 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Walking

                                                  5 kph (12 minutes per km) </td>

                                                <td class="bodyText" width="78"><div align="center">1.76 </div></td>

                                                <td class="bodyText"><div align="center">88 </div></td>

                                                <td class="bodyText"><div align="center">176 </div></td>

                                                <td class="bodyText"><div align="center">265 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Walking

                                                  6 kph (10 minutes per km) </td>

                                                <td class="bodyText" width="78"><div align="center">2.20 </div></td>

                                                <td class="bodyText"><div align="center">110 </div></td>

                                                <td class="bodyText"><div align="center">220 </div></td>

                                                <td class="bodyText"><div align="center">331 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Waterskiing </td>

                                                <td class="bodyText" width="78"><div align="center">3.53 </div></td>

                                                <td class="bodyText"><div align="center">176 </div></td>

                                                <td class="bodyText"><div align="center">353 </div></td>

                                                <td class="bodyText"><div align="center">529 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Weight

                                                  training fast </td>

                                                <td class="bodyText" width="78"><div align="center">5.62 </div></td>

                                                <td class="bodyText"><div align="center">281 </div></td>

                                                <td class="bodyText"><div align="center">562 </div></td>

                                                <td class="bodyText"><div align="center">843 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Weight

                                                  training mod </td>

                                                <td class="bodyText" width="78"><div align="center">4.19 </div></td>

                                                <td class="bodyText"><div align="center">209 </div></td>

                                                <td class="bodyText"><div align="center">419 </div></td>

                                                <td class="bodyText"><div align="center">628 </div></td>

                                              </tr>

                                              <tr height="35">

                                                <td width="255" height="35" class="bodyText">Weight

                                                  training slow </td>

                                                <td class="bodyText" width="78"><div align="center">2.76 </div></td>

                                                <td class="bodyText"><div align="center">138 </div></td>

                                                <td class="bodyText"><div align="center">276 </div></td>

                                                <td class="bodyText"><div align="center">413 </div></td>

                                              </tr>      
</tbody></table>
</div>