package com.slx.domain;

import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findSleepsByClientId" })
public class Sleep {

    @NotNull
    @ManyToOne
    private Client clientId;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar sleep_date;

    @NotNull
    @DecimalMin("0")
    @DecimalMax("24")
    private Float hours_slept;

    @NotNull
    @DecimalMin("0")
    @DecimalMax("10")
    private Float sleep_rating;

    private String sleep_comment;

    public static com.slx.domain.Sleep findSleepByUsername(String Username) {
        if (Username == null) return null;
        Client client = Client.findClientByUsername(Username);
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId", Sleep.class);
        q.setParameter("clientId", client);
        if (q.getResultList().isEmpty()) return null;
        return q.getSingleResult();
    }

    public static List<com.slx.domain.Sleep> findAllSleepsByUsername(String Username) {
        if (Username == null) return null;
        Client client = Client.findClientByUsername(Username);
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId ORDER BY o.sleep_date ASC", Sleep.class);
        q.setParameter("clientId", client);
        if (q.getResultList().isEmpty()) return null;
        return q.getResultList();
    }

    public static com.slx.domain.Sleep findSleepByClientAndId(Long id, Client clientId) {
        if (id == null || clientId == null) return null;
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId and o.id = :id", Sleep.class);
        q.setParameter("clientId", clientId);
        q.setParameter("id", id);
        return q.getSingleResult();
    }

    public static List<com.slx.domain.Sleep> findSleepEntriesByClientr(int firstResult, int maxResults, Client clientId) {
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId ORDER BY o.sleep_date ASC", Sleep.class);
        q.setParameter("clientId", clientId);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
        return q.getResultList();
    }

    public static List<com.slx.domain.Sleep> findAllSleepsByClient(Client clientId) {
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId ORDER BY o.sleep_date ASC", Sleep.class);
        q.setParameter("clientId", clientId);
        return q.getResultList();
    }

    public static long countSleepsByClient(Client clientId) {
        EntityManager em = Sleep.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Sleep o WHERE o.clientId = :clientId", Long.class);
        q.setParameter("clientId", clientId);
        return q.getSingleResult();
    }

    public static List<com.slx.domain.Sleep> findAllSleeps() {
        return entityManager().createQuery("SELECT o FROM Sleep o", Sleep.class).getResultList();
    }

    public static com.slx.domain.Sleep findSleep(Long id) {
        if (id == null) return null;
        return entityManager().find(Sleep.class, id);
    }

    public static List<com.slx.domain.Sleep> findSleepEntries(int firstResult, int maxResults, String Username) {
        Client client = Client.findClientByUsername(Username);
        List<Sleep> res = entityManager().createQuery("SELECT o FROM Sleep o WHERE o.clientId = :clientId", Sleep.class).setParameter("clientId", client).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
        if (res.isEmpty()) return null;
        return res;
    }

    public static List<com.slx.domain.Sleep> findSleepEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Sleep o", Sleep.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

	public static Sleep findSleepsByClientId(Client clientId) {
        if (clientId == null) throw new IllegalArgumentException("The clientId argument is required");
        EntityManager em = Sleep.entityManager();
        TypedQuery<Sleep> q = em.createQuery("SELECT o FROM Sleep AS o WHERE o.clientId = :clientId", Sleep.class);
        q.setParameter("clientId", clientId);
        return (Sleep) q;
    }
}
