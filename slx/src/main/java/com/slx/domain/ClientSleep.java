package com.slx.domain;


import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.OneToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ClientSleep {

    @NotNull
    @OneToOne
    private Client clientId;

    @NotNull
    @DecimalMin("4")
    @DecimalMax("16")
    private float sleep_target;

    public void calculateAge(){
    	int year =clientId.getProfile().getBirthdate().get(Calendar.YEAR);
    	int month=clientId.getProfile().getBirthdate().get(Calendar.MONTH);
    	int day = clientId.getProfile().getBirthdate().get(Calendar.DAY_OF_MONTH);
    	
    	DateMidnight birthdate = new DateMidnight(year, month, day);
    	DateTime now = new DateTime();
    	Years age = Years.yearsBetween(birthdate, now);
        System.out.println("***CURRENT AGE OF "+ clientId + " >> "+ age);
    }
    
    public static ClientSleep findClientSleepByUsername(String Username) {
        if (Username == null) return null;
        Client client = Client.findClientByUsername(Username);
        EntityManager em = ClientSleep.entityManager();
        TypedQuery<ClientSleep> q = em.createQuery("SELECT o FROM ClientSleep AS o WHERE o.clientId = :clientId", ClientSleep.class);
        q.setParameter("clientId", client);
        if (q.getResultList().isEmpty()) return null;
        return q.getSingleResult();
    }

}
