package com.slx.domain;


import javax.persistence.Column;
import javax.persistence.EntityManager;

import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Client {

    @NotNull
    @Column(unique = true)
    @Size(min = 4, max = 16)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String username;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;

    @OneToOne
    private Profile profile;

    @PrePersist
    @PreUpdate
    protected void encryptPassword() {
        password = DigestUtils.md5Hex(password);
    }


	
	public static Client findClientByUsername(String Username) {
        if (Username == null) return null;
        EntityManager em = Client.entityManager();
        TypedQuery<Client> q = em.createQuery("SELECT o FROM Client AS o WHERE o.username = :username", Client.class);
        q.setParameter("username", Username);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getSingleResult();
    }
	
	
}
