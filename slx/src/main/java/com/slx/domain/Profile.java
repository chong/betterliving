package com.slx.domain;

import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findProfilesByClient" })
public class Profile {

    @NotNull
    @OneToOne
    private Client clientId;

    @NotNull
    @Size(min = 2, max = 32)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String first_name;

    @NotNull
    @Size(min = 2, max = 32)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String last_name;

    @NotNull
    @Enumerated
    private Gender gender;

    @Pattern(regexp = "[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+")
    private String email;

    @NotNull
    @Past
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar birthdate;

    public static Profile findProfileByUsername(String Username) {
        if (Username == null) return null;
        Client client = Client.findClientByUsername(Username);
        EntityManager em = Profile.entityManager();
        TypedQuery<Profile> q = em.createQuery("SELECT o FROM Profile AS o WHERE o.clientId = :clientId", Profile.class);
        q.setParameter("clientId", client);
        if (q.getResultList().isEmpty()) return null;
        return q.getSingleResult();
    }
}
