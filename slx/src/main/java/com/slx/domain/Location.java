package com.slx.domain;

import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.TypedQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Location {

    @NotNull
    @OneToOne
    private Client clientId;

    @NotNull 
    private String city;
    
    @NotNull
    @Enumerated
    private Countries country; 
    
    @NotNull
    @Min(0L)
    @Max(9999L)
    private int postcode;

    
    public static Location findLocationByUsername(String Username) {
    	if (Username == null) return null;
    	Client client = Client.findClientByUsername(Username);
        EntityManager em = Location.entityManager();
        TypedQuery<Location> q = em.createQuery("SELECT o FROM Location AS o WHERE o.clientId = :clientId", Location.class);
        q.setParameter("clientId", client);
        if(q.getResultList().isEmpty())
        	return null;
        return q.getSingleResult();
    }
}
