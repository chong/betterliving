package com.slx.domain;


public enum Countries {

    America, Argentina, Australia, Brazil, China, Columbia, Denmark, Ecuador, France, Germany, Holland, India, Japan, Korea, Libya, Malta, Mexico, New_Zealand, Oman, Pakistan, Poland, Qatar, Russia, Serbia, Sweden, Turkey, Ukraine, Vietnam, Western_Samoa, Yemen, Zambia;
}
