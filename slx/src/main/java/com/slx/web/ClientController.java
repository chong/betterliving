package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.Profile;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.context.annotation.Scope;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@RequestMapping("/clients")
@Controller
@RooWebScaffold(path = "clients", formBackingObject = Client.class)
@SessionAttributes("newclient")
@Scope("session")
public class ClientController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Client client, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, client);
            return "clients/create";
        }
        uiModel.asMap().clear();
        client.persist();
        List<Client> newclient = new ArrayList<Client>();
        newclient.add(client);
        uiModel.addAttribute("newclient", newclient);
        System.out.println("adding new client>>" + client);
        return "redirect:/profiles?form";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("clients", Client.findClientEntries(firstResult, sizeNo));
            float nrOfPages = (float) Client.countClients() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("clients", Client.findAllClients());
        }
        return "clients/list";
    }

    void populateEditForm(Model uiModel, Client client) {
        uiModel.addAttribute("client", client);
        uiModel.addAttribute("profiles", Profile.findAllProfiles());
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("client", Client.findClient(id));
        uiModel.addAttribute("itemId", id);
        return "clients/show";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Client client, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, client);
            return "clients/update";
        }
        uiModel.asMap().clear();
        client.merge();
        return "redirect:/clients/" + encodeUrlPathSegment(client.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Client.findClient(id));
        return "clients/update";
    }
}
