package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.ClientSleep;
import com.slx.domain.Sleep;


import java.security.Principal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/sleeps")
@Controller
@RooWebScaffold(path = "sleeps", formBackingObject = Sleep.class)
public class SleepController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Sleep sleep, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, sleep);
            return "sleeps/create";
        }
        uiModel.asMap().clear();
        sleep.persist();
        System.out.println("sleep created with id- "+ sleep.getId());
        return "redirect:/sleeps/" + encodeUrlPathSegment(sleep.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel, Principal principal) {
		  String username = principal.getName();
	        System.out.println("add sleep principal: " + username);
	        Client client= Client.findClientByUsername(username);

	        List<Client> newsleep = new ArrayList<Client>();
	        newsleep.add(client);
	        uiModel.addAttribute("newsleep", newsleep);
	        System.out.println("addin sleep newsleep: " );
	        
        populateEditForm(uiModel, new Sleep());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Client.countClients() == 0) {
            dependencies.add(new String[] { "client", "clients" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "sleeps/create";
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Sleep sleep = Sleep.findSleep(id);
        sleep.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/sleeps";
    }

/*
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel,Principal principal) {
        uiModel.addAttribute("sleeps", Sleep.findAllSleepsByUsername(principal.getName()));
        addDateTimeFormatPatterns(uiModel);
        return "sleeps/list";
    }
*/
	
	
	void populateEditForm(Model uiModel, Sleep sleep) {
        uiModel.addAttribute("sleep", sleep);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("clients", Client.findAllClients());
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("sleep", Sleep.findSleep(id));
        uiModel.addAttribute("itemId", id);
        return "sleeps/show";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Sleep sleep, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
     
		if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, sleep);
            return "sleeps/update";
        }
        uiModel.asMap().clear();
        sleep.merge();
        return "redirect:/sleeps/" + encodeUrlPathSegment(sleep.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
			 
			 uiModel.addAttribute("sleep", Sleep.findSleep(id));
		     uiModel.addAttribute("itemId", id);
		     
		     Client client= Client.findClientByUsername(principal.getName());
			   List<Client> updateClientId = new ArrayList<Client>();
			   updateClientId.add(client);
			  uiModel.addAttribute("updateClientId", updateClientId);
		
        populateEditForm(uiModel, Sleep.findSleep(id));
        return "sleeps/update";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel,Principal principal) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("sleeps", Sleep.findSleepEntriesByClientr(firstResult, sizeNo,Client.findClientByUsername(principal.getName())));
            float nrOfPages = (float) Sleep.countSleeps() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            //uiModel.addAttribute("sleeps", Sleep.findAllSleeps());
        	uiModel.addAttribute("sleeps",Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName() ) ) );
        
        } 
		
/////		List<Sleep> sleeps= Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName() ) );
/////		uiModel.addAttribute("sleeps", sleeps );
		
        addDateTimeFormatPatterns(uiModel);
        return "sleeps/list";
    }
	
	////////////////CHARTS STUFFFFFF
	
	@RequestMapping(params = "showAllSleepChart", produces = "text/html")
    public String showAllSleepChart() {
        return "sleeps/showAllSleepChart";
	}
	
	@RequestMapping(params = "getAllSleepChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getAllSleepChartData() {
		List<Sleep> sleepData = Sleep.findAllSleeps();
		System.out.println("sleep data"+ sleepData);
		
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(2);
		
		ModelMap data = new ModelMap();
		List<ModelMap> cols = new ArrayList<ModelMap>();
		ModelMap col = new ModelMap();
		col.addAttribute("label", "Label");
		col.addAttribute("type", "string");
		cols.add((ModelMap)col.clone());		
		col.addAttribute("label", "Hours Slept");
		col.addAttribute("type", "number");
		cols.add((ModelMap)col.clone());
		data.addAttribute("cols", cols);
		List<ModelMap> rows = new ArrayList<ModelMap>();
		Iterator<Sleep> it = sleepData.iterator();
		int num = 0;
		while(it.hasNext())
		{
			num++;
			Sleep sleep = (Sleep) it.next();
			ModelMap row = new ModelMap();
			List<ModelMap> cells = new ArrayList<ModelMap>();
			ModelMap cell = new ModelMap();
			
			Calendar date = Calendar.getInstance();
			date=sleep.getSleep_date();
		/*	
			StringBuilder sb = new StringBuilder();
			sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
			append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
			
			cell.addAttribute("v", sb.toString());
			
			*/
			cell.addAttribute("v", "");
			cells.add((ModelMap) cell.clone());			
			
			cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
			cells.add((ModelMap) cell.clone());
			row.addAttribute("c", cells);
			rows.add(row);
		}
		data.addAttribute("rows", rows);
		return data;
	}
	
	/////////
	@RequestMapping(params = "showClientSleepChart", produces = "text/html")
    public String showClientSleepChart() {
        return "sleeps/showClientSleepChart";
	}
	
	@RequestMapping(params = "getClientSleepChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getClientSleepChartData(Principal principal) {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleepsByUsername(principal.getName());
		
		System.out.println("user sleeps data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

				Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
				
				cell.addAttribute("v", sb.toString());
				cells.add((ModelMap) cell.clone());
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	
	@RequestMapping(params = "showClientCompChart", produces = "text/html")
    public String showClientCompChart() {
        return "sleeps/showClientCompChart";
	}
	
	@RequestMapping(params = "getClientCompChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getClientCompChartData(Principal principal) {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleepsByUsername(principal.getName());
		float sleepTarget = ClientSleep.findClientSleepByUsername(principal.getName()).getSleep_target();
		
		System.out.println("user sleeps hour data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			col.addAttribute("label", "Target Hours");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

				Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
				
				cell.addAttribute("v", sb.toString());
				cells.add((ModelMap) cell.clone());
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());
				
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleepTarget)));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	@RequestMapping(params = "showAllCompChart", produces = "text/html")
    public String showAllCompChart() {
        return "sleeps/showAllCompChart";
	}
	
	@RequestMapping(params = "getAllCompChartData2", method = RequestMethod.GET)
	public @ResponseBody ModelMap getAllCompChartData2() {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleeps();
		
		
		System.out.println("user sleeps hour data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			col.addAttribute("label", "Target Hours");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

				Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
				
				cell.addAttribute("v", sb.toString());
				cells.add((ModelMap) cell.clone());
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());				
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	
	@RequestMapping(params = "getAllCompChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getAllCompChartData() {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleeps();
		
		float average=0;
		int count = sleepData.size();
		for (int i=0; i< sleepData.size();i++)
		{
		average = average + sleepData.get(i).getSleep_rating();	
		}
		if (count>0)
		{average = average/count;}	
		
		System.out.println("user sleeps hour data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			col.addAttribute("label", "Target Hours");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

				Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
				
				cell.addAttribute("v", sb.toString());
				cells.add((ModelMap) cell.clone());
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());				
				
				cell.addAttribute("v", Float.parseFloat(format.format(average)));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	@RequestMapping(params = "getClientComboChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getClientComboChartData(Principal principal) {
		List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		//List<Sleep> sleepData = Sleep.findAllSleeps();
		
		System.out.println("user sleeps hour data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Sleep Rating");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getSleep_rating())));
				cells.add((ModelMap) cell.clone());				
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	
	@RequestMapping(params = "getAllComboChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getAllComboChartData(Principal principal) {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleeps();
		
		System.out.println("user sleeps hour data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Sleep Rating");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			col.addAttribute("label", "Hours Slept");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getSleep_rating())));
				cells.add((ModelMap) cell.clone());				
				
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getHours_slept())));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	
	@RequestMapping(params = "showClientRatingChart", produces = "text/html")
    public String showClientRatingChart() {
        return "sleeps/showClientRatingChart";
	}
	
	@RequestMapping(params = "showClientComboChart", produces = "text/html")
    public String showClientComboChart() {
        return "sleeps/showClientComboChart";
	}
	
	@RequestMapping(params = "showAllComboChart", produces = "text/html")
    public String showAllComboChart() {
        return "sleeps/showAllComboChart";
	}
	
	@RequestMapping(params = "getClientRatingChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getClientRatingChartData(Principal principal) {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleepsByUsername(principal.getName());
	
		NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Rating");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

				Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
				
				cell.addAttribute("v", sb.toString());
				cells.add((ModelMap) cell.clone());
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getSleep_rating())));
				cells.add((ModelMap) cell.clone());
				
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}
	
	
	
	
	@RequestMapping(params = "showAllRatingChart", produces = "text/html")
    public String showAllRatingChart() {
        return "sleeps/showAllRatingChart";
	}
	
	@RequestMapping(params = "getAllRatingChartData", method = RequestMethod.GET)
	public @ResponseBody ModelMap getAllRatingChartData() {
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		//List<Sleep> sleepData = Sleep.findAllSleepsByClient(Client.findClientByUsername(principal.getName()));
		List<Sleep> sleepData = Sleep.findAllSleeps();
		
		System.out.println("user sleeps data"+ sleepData);

			
			NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(2);
			
			ModelMap data = new ModelMap();
			List<ModelMap> cols = new ArrayList<ModelMap>();
			ModelMap col = new ModelMap();
			col.addAttribute("label", "Label");
			col.addAttribute("type", "string");
			cols.add((ModelMap)col.clone());		
			col.addAttribute("label", "Rating");
			col.addAttribute("type", "number");
			cols.add((ModelMap)col.clone());
			
			data.addAttribute("cols", cols);
			List<ModelMap> rows = new ArrayList<ModelMap>();
			Iterator<Sleep> it = sleepData.iterator();
			int num = 0;
			while(it.hasNext())
			{
				num++;
				Sleep sleep = (Sleep) it.next();
				ModelMap row = new ModelMap();
				List<ModelMap> cells = new ArrayList<ModelMap>();
				ModelMap cell = new ModelMap();

			/*	Calendar date = Calendar.getInstance();
				date=sleep.getSleep_date();
				
				StringBuilder sb = new StringBuilder();
				sb.append("Date(").append(date.get(Calendar.YEAR)).append(",").
				append(date.get(Calendar.MONTH)).append(",").append(date.get(Calendar.DATE)).append(")");
			
				cell.addAttribute("v", sb.toString());  */	
				cell.addAttribute("v", ""); 
				cells.add((ModelMap) cell.clone());
				cell.addAttribute("v", Float.parseFloat(format.format(sleep.getSleep_rating())));
				cells.add((ModelMap) cell.clone());
				row.addAttribute("c", cells);
				rows.add(row);
			}
			data.addAttribute("rows", rows);
			return data;
		}

	

	
	
}
