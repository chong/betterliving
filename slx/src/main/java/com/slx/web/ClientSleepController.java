package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.ClientSleep;


import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

@RequestMapping("/clientsleeps")
@Controller
@RooWebScaffold(path = "clientsleeps", formBackingObject = ClientSleep.class)
public class ClientSleepController {

	 @RequestMapping(params = "showSleepTarget", produces = "text/html")
	    public String showSleepTarget(Model uiModel, Principal principal) {   
	        System.out.println("SHOW CLIENT SLEEP TARGET");
		    uiModel.addAttribute("clientSleep",  ClientSleep.findClientSleepByUsername(principal.getName()));      	        
	        return "clientsleeps/show";
	    }
	 
	  @RequestMapping(params = "updateSleepTarget", produces = "text/html")
	    public String updateSleepTarget(Model uiModel, Principal principal) {
		  System.out.println("******UPDATE CLIENT SLEEP TARGET>> maybe....");
		  ClientSleep clientSleep = ClientSleep.findClientSleepByUsername(principal.getName());
		  uiModel.addAttribute("clientSleep", clientSleep);
		      Client client= clientSleep.getClientId();
			  List<Client> updateClientId = new ArrayList<Client>();
			  updateClientId.add(client);
			  uiModel.addAttribute("updateClientId", updateClientId);
			  
			  System.out.println("bbbxxxyyUPDATE CLIENT SLEEP TARGET>> " + clientSleep);
	        System.out.println("xxxyyUPDATE CLIENT SLEEP TARGET>> ");
	        return "clientsleeps/update";
	    }
	
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid ClientSleep clientSleep, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
			if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, clientSleep);
            System.out.println("add cs binding err: " );
            return "clientsleeps/create";
        }
        uiModel.asMap().clear();
        clientSleep.persist();
        System.out.println("clientsleep created with id- "+ clientSleep.getId() + " now get ready to login..");
    // return "redirect:/clientsleeps?showSleepTarget";
     //return "redirect:/clientsleeps/" + encodeUrlPathSegment(clientSleep.getId().toString(), httpServletRequest);
        return "clients/success";
    }
	
	
	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new ClientSleep());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Client.countClients() == 0) {
            dependencies.add(new String[] { "client", "clients" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "clientsleeps/create";
    }
	

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }

	void populateEditForm(Model uiModel, ClientSleep clientSleep) {
        uiModel.addAttribute("clientSleep", clientSleep);
        uiModel.addAttribute("clients", Client.findAllClients());
       

    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid ClientSleep clientSleep, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
       
			        
		if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, clientSleep);
            System.out.println("error binding clientsleep: " );
            return "clientsleeps/update";
        }
       // uiModel.asMap().clear();
        clientSleep.merge();
      
        System.out.println("update *** ClientId: " );        
        
        return "redirect:/clientsleeps/" + encodeUrlPathSegment(clientSleep.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel, Principal principal) {
		 String username = principal.getName();
		 uiModel.addAttribute("clientsleep", ClientSleep.findClientSleep(id));
	     uiModel.addAttribute("itemId", id);
	     
	     Client client= Client.findClientByUsername(principal.getName());
		   List<Client> updateClientId = new ArrayList<Client>();
		   updateClientId.add(client);
		  uiModel.addAttribute("updateClientId", updateClientId);
	        
       populateEditForm(uiModel, ClientSleep.findClientSleepByUsername(username));
           System.out.println("updateClientSleepform >>" );
        return "clientsleeps/update";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("clientsleep", ClientSleep.findClientSleep(id));
        uiModel.addAttribute("itemId", id);
        return "clientsleeps/show";
    }


}
