package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.Gender;
import com.slx.domain.Profile;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/profiles")
@Controller
@RooWebScaffold(path = "profiles", formBackingObject = Profile.class)
public class ProfileController {

    @RequestMapping(params = "showProfile", produces = "text/html")
    public String showProfile(Model uiModel, Principal principal) {
        uiModel.addAttribute("profile", Profile.findProfileByUsername(principal.getName()));
        System.out.println("SHOW PROFILE");
        return "profiles/show";
    }

    @RequestMapping(params = "getProfile", produces = "text/html")
    public String getProfile(Model uiModel, Principal principal) {
        uiModel.addAttribute("profile", Profile.findProfileByUsername(principal.getName()));
        System.out.println("GET PROFILE");
        return "profiles/update";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Profile profile, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, profile);
            return "profiles/update";
        }
        uiModel.asMap().clear();
        profile.merge();
        return "redirect:/profiles/" + encodeUrlPathSegment(profile.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Profile.findProfile(id));
        return "profiles/update";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Profile profile, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, profile);
            System.out.println("PROFILE CREATE ERROR");
            return "profiles/create";
        }
        uiModel.asMap().clear();
        profile.persist();
        System.out.println("PROFILE CREATE OK....now onto location!!!");
        return "redirect:locations?form";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Profile());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Client.countClients() == 0) {
            dependencies.add(new String[] { "client", "clients" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "profiles/create";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("profiles", Profile.findProfileEntries(firstResult, sizeNo));
            float nrOfPages = (float) Profile.countProfiles() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("profiles", Profile.findAllProfiles());
        }
        return "profiles/list";
    }

    void populateEditForm(Model uiModel, Profile profile) {
        uiModel.addAttribute("profile", profile);
        uiModel.addAttribute("clients", Client.findAllClients());
        uiModel.addAttribute("genders", Arrays.asList(Gender.values()));
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("profile", Profile.findProfile(id));
        uiModel.addAttribute("itemId", id);
        return "profiles/show";
    }
}
