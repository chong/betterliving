package com.slx.web;

import com.slx.domain.Client;
import com.slx.domain.Countries;
import com.slx.domain.Location;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/locations")
@Controller
@RooWebScaffold(path = "locations", formBackingObject = Location.class)
public class LocationController {

    @RequestMapping(params = "showLocation", produces = "text/html")
    public String showLocation(Model uiModel, Principal principal) {
        uiModel.addAttribute("location", Location.findLocationByUsername(principal.getName()));
        System.out.println("SHOW LOCATION");
        return "locations/show";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Location location, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, location);
            System.out.println("Location created FAILED");
            return "locations/create";
        }
        uiModel.asMap().clear();
        location.persist();
        System.out.println("Location created...now onto sleep target ");
        //System.out.println("Location created... now login");
        //return "clients/success";
        return "redirect:clientsleeps?form";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Location());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Client.countClients() == 0) {
            dependencies.add(new String[] { "client", "clients" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        return "locations/create";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("locations", Location.findLocationEntries(firstResult, sizeNo));
            float nrOfPages = (float) Location.countLocations() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("locations", Location.findAllLocations());
        }
        return "locations/list";
    }

    void populateEditForm(Model uiModel, Location location) {
        uiModel.addAttribute("location", location);
        uiModel.addAttribute("clients", Client.findAllClients());
        uiModel.addAttribute("countrieses", Arrays.asList(Countries.values()));
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("location", Location.findLocation(id));
        uiModel.addAttribute("itemId", id);
        return "locations/show";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Location location, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, location);
            return "locations/update";
        }
        uiModel.asMap().clear();
        location.merge();
        return "redirect:/locations/" + encodeUrlPathSegment(location.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Location.findLocation(id));
        return "locations/update";
    }
}
