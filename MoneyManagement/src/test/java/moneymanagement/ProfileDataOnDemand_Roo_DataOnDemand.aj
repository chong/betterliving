// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package moneymanagement;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import moneymanagement.Profile;
import moneymanagement.ProfileDataOnDemand;
import org.springframework.stereotype.Component;

privileged aspect ProfileDataOnDemand_Roo_DataOnDemand {
    
    declare @type: ProfileDataOnDemand: @Component;
    
    private Random ProfileDataOnDemand.rnd = new SecureRandom();
    
    private List<Profile> ProfileDataOnDemand.data;
    
    public Profile ProfileDataOnDemand.getNewTransientProfile(int index) {
        Profile obj = new Profile();
        setAge(obj, index);
        setEmail(obj, index);
        setFirst_name(obj, index);
        setGender(obj, index);
        setLast_name(obj, index);
        setPassword(obj, index);
        setUsername(obj, index);
        return obj;
    }
    
    public void ProfileDataOnDemand.setAge(Profile obj, int index) {
        short age = new Integer(index).shortValue();
        if (age < 18 || age > 120) {
            age = 120;
        }
        obj.setAge(age);
    }
    
    public void ProfileDataOnDemand.setEmail(Profile obj, int index) {
        String email = "foo" + index + "@bar.com";
        obj.setEmail(email);
    }
    
    public void ProfileDataOnDemand.setFirst_name(Profile obj, int index) {
        String first_name = "first_name_" + index;
        obj.setFirst_name(first_name);
    }
    
    public void ProfileDataOnDemand.setGender(Profile obj, int index) {
        char gender = 'N';
        obj.setGender(gender);
    }
    
    public void ProfileDataOnDemand.setLast_name(Profile obj, int index) {
        String last_name = "last_name_" + index;
        obj.setLast_name(last_name);
    }
    
    public void ProfileDataOnDemand.setPassword(Profile obj, int index) {
        String password = "password_" + index;
        if (password.length() > 32) {
            password = password.substring(0, 32);
        }
        obj.setPassword(password);
    }
    
    public void ProfileDataOnDemand.setUsername(Profile obj, int index) {
        String username = "username_" + index;
        if (username.length() > 16) {
            username = new Random().nextInt(10) + username.substring(1, 16);
        }
        obj.setUsername(username);
    }
    
    public Profile ProfileDataOnDemand.getSpecificProfile(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Profile obj = data.get(index);
        Long id = obj.getId();
        return Profile.findProfile(id);
    }
    
    public Profile ProfileDataOnDemand.getRandomProfile() {
        init();
        Profile obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return Profile.findProfile(id);
    }
    
    public boolean ProfileDataOnDemand.modifyProfile(Profile obj) {
        return false;
    }
    
    public void ProfileDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = Profile.findProfileEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Profile' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Profile>();
        for (int i = 0; i < 10; i++) {
            Profile obj = getNewTransientProfile(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
