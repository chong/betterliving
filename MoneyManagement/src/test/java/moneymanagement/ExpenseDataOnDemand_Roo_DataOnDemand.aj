// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package moneymanagement;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import moneymanagement.Expense;
import moneymanagement.ExpenseDataOnDemand;
import org.springframework.stereotype.Component;

privileged aspect ExpenseDataOnDemand_Roo_DataOnDemand {
    
    declare @type: ExpenseDataOnDemand: @Component;
    
    private Random ExpenseDataOnDemand.rnd = new SecureRandom();
    
    private List<Expense> ExpenseDataOnDemand.data;
    
    public Expense ExpenseDataOnDemand.getNewTransientExpense(int index) {
        Expense obj = new Expense();
        setAmount(obj, index);
        setExpense_id(obj, index);
        setExpense_name(obj, index);
        set_date(obj, index);
        return obj;
    }
    
    public void ExpenseDataOnDemand.setAmount(Expense obj, int index) {
        Float amount = new Integer(index).floatValue();
        obj.setAmount(amount);
    }
    
    public void ExpenseDataOnDemand.setExpense_id(Expense obj, int index) {
        Integer expense_id = new Integer(index);
        obj.setExpense_id(expense_id);
    }
    
    public void ExpenseDataOnDemand.setExpense_name(Expense obj, int index) {
        String expense_name = "expense_name_" + index;
        obj.setExpense_name(expense_name);
    }
    
    public void ExpenseDataOnDemand.set_date(Expense obj, int index) {
        Calendar _date = Calendar.getInstance();
        obj.set_date(_date);
    }
    
    public Expense ExpenseDataOnDemand.getSpecificExpense(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Expense obj = data.get(index);
        Long id = obj.getId();
        return Expense.findExpense(id);
    }
    
    public Expense ExpenseDataOnDemand.getRandomExpense() {
        init();
        Expense obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return Expense.findExpense(id);
    }
    
    public boolean ExpenseDataOnDemand.modifyExpense(Expense obj) {
        return false;
    }
    
    public void ExpenseDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = Expense.findExpenseEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Expense' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Expense>();
        for (int i = 0; i < 10; i++) {
            Expense obj = getNewTransientExpense(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
