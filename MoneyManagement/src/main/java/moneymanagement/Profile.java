package moneymanagement;

import javax.persistence.Column;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Profile {

    @NotNull
    private String first_name;

    @NotNull
    private String last_name;

    @Min(18L)
    @Max(120L)
    private short age;

    private char gender;

    private String email;
    
    @NotNull
    @Column(unique = true)
    @Size(min = 4, max = 16)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String username;

    @NotNull
    @Size(min = 4, max = 32)
    private String password;
    
    @PrePersist
    @PreUpdate
    protected void encryptPassword() {
        password = DigestUtils.md5Hex(password);
    }
}
