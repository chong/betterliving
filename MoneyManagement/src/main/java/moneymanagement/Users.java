package moneymanagement;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import moneymanagement.Profile;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Users {

    @NotNull
    @Column(unique = true)
    @Size(min = 6, max = 16)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String username;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;

    @OneToOne
    private Profile profile;

    @PrePersist
    @PreUpdate
    protected void encryptPassword() {
        password = DigestUtils.md5Hex(password);
    }
}
