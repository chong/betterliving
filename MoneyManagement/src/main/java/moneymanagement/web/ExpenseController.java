package moneymanagement.web;

import moneymanagement.Expense;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/expenses")
@Controller
@RooWebScaffold(path = "expenses", formBackingObject = Expense.class)
public class ExpenseController {
}
