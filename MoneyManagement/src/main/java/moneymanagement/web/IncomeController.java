package moneymanagement.web;

import moneymanagement.Income;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/incomes")
@Controller
@RooWebScaffold(path = "incomes", formBackingObject = Income.class)
public class IncomeController {
}
