package mooddiary.domain;

import java.util.Calendar;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MoodEntry {

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar dateLogged = Calendar.getInstance();

    @Enumerated
    private Rating currentMood;

    @Enumerated
    private Rating dayMood;

    @Enumerated
    private Emotion predominantEmotion;

}
