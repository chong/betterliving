package mooddiary.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MoodTrend {

    private Boolean suddenDrop;

    private Boolean isUpset;

    private double avgRatingForPeriod;
}
