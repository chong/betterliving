package mooddiary.domain;


public enum Emotion {

    HAPPY, ANGRY, SAD, ANXIOUS, APATHETIC;
}
