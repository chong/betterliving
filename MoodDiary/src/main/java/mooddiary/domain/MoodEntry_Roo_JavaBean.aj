// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package mooddiary.domain;

import java.util.Calendar;
import mooddiary.domain.Emotion;
import mooddiary.domain.MoodEntry;
import mooddiary.domain.Rating;

privileged aspect MoodEntry_Roo_JavaBean {
    
    public Calendar MoodEntry.getDateLogged() {
        return this.dateLogged;
    }
    
    public void MoodEntry.setDateLogged(Calendar dateLogged) {
        this.dateLogged = dateLogged;
    }
    
    public Rating MoodEntry.getCurrentMood() {
        return this.currentMood;
    }
    
    public void MoodEntry.setCurrentMood(Rating currentMood) {
        this.currentMood = currentMood;
    }
    
    public Rating MoodEntry.getDayMood() {
        return this.dayMood;
    }
    
    public void MoodEntry.setDayMood(Rating dayMood) {
        this.dayMood = dayMood;
    }
    
    public Emotion MoodEntry.getPredominantEmotion() {
        return this.predominantEmotion;
    }
    
    public void MoodEntry.setPredominantEmotion(Emotion predominantEmotion) {
        this.predominantEmotion = predominantEmotion;
    }
    
}
