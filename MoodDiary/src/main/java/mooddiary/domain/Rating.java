package mooddiary.domain;


public enum Rating {

    AWFUL, UNPLEASANT, OKAY, GOOD, AWESOME;
}
