// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package mooddiary.web;

import mooddiary.domain.Suggestion;
import mooddiary.web.SuggestionController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

privileged aspect SuggestionController_Roo_Controller {
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String SuggestionController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("suggestion", Suggestion.findSuggestion(id));
        uiModel.addAttribute("itemId", id);
        return "suggestions/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String SuggestionController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("suggestions", Suggestion.findSuggestionEntries(firstResult, sizeNo));
            float nrOfPages = (float) Suggestion.countSuggestions() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("suggestions", Suggestion.findAllSuggestions());
        }
        return "suggestions/list";
    }
    
}
