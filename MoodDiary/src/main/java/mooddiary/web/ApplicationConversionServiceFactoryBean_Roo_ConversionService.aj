// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package mooddiary.web;

import mooddiary.domain.MoodEntry;
import mooddiary.domain.MoodTrend;
import mooddiary.domain.Person;
import mooddiary.domain.Suggestion;
import mooddiary.web.ApplicationConversionServiceFactoryBean;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

privileged aspect ApplicationConversionServiceFactoryBean_Roo_ConversionService {
    
    declare @type: ApplicationConversionServiceFactoryBean: @Configurable;
    
    public Converter<MoodEntry, String> ApplicationConversionServiceFactoryBean.getMoodEntryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<mooddiary.domain.MoodEntry, java.lang.String>() {
            public String convert(MoodEntry moodEntry) {
                return new StringBuilder().append(moodEntry.getDateLogged()).toString();
            }
        };
    }
    
    public Converter<Long, MoodEntry> ApplicationConversionServiceFactoryBean.getIdToMoodEntryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, mooddiary.domain.MoodEntry>() {
            public mooddiary.domain.MoodEntry convert(java.lang.Long id) {
                return MoodEntry.findMoodEntry(id);
            }
        };
    }
    
    public Converter<String, MoodEntry> ApplicationConversionServiceFactoryBean.getStringToMoodEntryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, mooddiary.domain.MoodEntry>() {
            public mooddiary.domain.MoodEntry convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), MoodEntry.class);
            }
        };
    }
    
    public Converter<MoodTrend, String> ApplicationConversionServiceFactoryBean.getMoodTrendToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<mooddiary.domain.MoodTrend, java.lang.String>() {
            public String convert(MoodTrend moodTrend) {
                return new StringBuilder().append(moodTrend.getAvgRatingForPeriod()).toString();
            }
        };
    }
    
    public Converter<Long, MoodTrend> ApplicationConversionServiceFactoryBean.getIdToMoodTrendConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, mooddiary.domain.MoodTrend>() {
            public mooddiary.domain.MoodTrend convert(java.lang.Long id) {
                return MoodTrend.findMoodTrend(id);
            }
        };
    }
    
    public Converter<String, MoodTrend> ApplicationConversionServiceFactoryBean.getStringToMoodTrendConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, mooddiary.domain.MoodTrend>() {
            public mooddiary.domain.MoodTrend convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), MoodTrend.class);
            }
        };
    }
    
    public Converter<Person, String> ApplicationConversionServiceFactoryBean.getPersonToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<mooddiary.domain.Person, java.lang.String>() {
            public String convert(Person person) {
                return new StringBuilder().append(person.getUsername()).append(' ').append(person.getPassword()).toString();
            }
        };
    }
    
    public Converter<Long, Person> ApplicationConversionServiceFactoryBean.getIdToPersonConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, mooddiary.domain.Person>() {
            public mooddiary.domain.Person convert(java.lang.Long id) {
                return Person.findPerson(id);
            }
        };
    }
    
    public Converter<String, Person> ApplicationConversionServiceFactoryBean.getStringToPersonConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, mooddiary.domain.Person>() {
            public mooddiary.domain.Person convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Person.class);
            }
        };
    }
    
    public Converter<Suggestion, String> ApplicationConversionServiceFactoryBean.getSuggestionToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<mooddiary.domain.Suggestion, java.lang.String>() {
            public String convert(Suggestion suggestion) {
                return new StringBuilder().append(suggestion.getEmoName()).append(' ').append(suggestion.getSuggestionId()).append(' ').append(suggestion.getSuggestion()).toString();
            }
        };
    }
    
    public Converter<Long, Suggestion> ApplicationConversionServiceFactoryBean.getIdToSuggestionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, mooddiary.domain.Suggestion>() {
            public mooddiary.domain.Suggestion convert(java.lang.Long id) {
                return Suggestion.findSuggestion(id);
            }
        };
    }
    
    public Converter<String, Suggestion> ApplicationConversionServiceFactoryBean.getStringToSuggestionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, mooddiary.domain.Suggestion>() {
            public mooddiary.domain.Suggestion convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Suggestion.class);
            }
        };
    }
    
    public void ApplicationConversionServiceFactoryBean.installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getMoodEntryToStringConverter());
        registry.addConverter(getIdToMoodEntryConverter());
        registry.addConverter(getStringToMoodEntryConverter());
        registry.addConverter(getMoodTrendToStringConverter());
        registry.addConverter(getIdToMoodTrendConverter());
        registry.addConverter(getStringToMoodTrendConverter());
        registry.addConverter(getPersonToStringConverter());
        registry.addConverter(getIdToPersonConverter());
        registry.addConverter(getStringToPersonConverter());
        registry.addConverter(getSuggestionToStringConverter());
        registry.addConverter(getIdToSuggestionConverter());
        registry.addConverter(getStringToSuggestionConverter());
    }
    
    public void ApplicationConversionServiceFactoryBean.afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
    
}
