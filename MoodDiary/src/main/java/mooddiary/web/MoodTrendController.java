package mooddiary.web;

import mooddiary.domain.MoodTrend;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/moodtrends")
@Controller
@RooWebScaffold(path = "moodtrends", formBackingObject = MoodTrend.class, delete=false, update=false, create=false, exposeFinders=false, exposeJson=false)
public class MoodTrendController {
}
