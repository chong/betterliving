package mooddiary.web;

import mooddiary.domain.Emotion;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/emotions")
@Controller
@RooWebScaffold(path = "emotions", formBackingObject = Emotion.class, delete=false, update=false, create=false, exposeFinders=false, exposeJson=false)
public class EmotionController {
}

//original path: FortyTwo\src\main\java\mooddiary\web\EmotionController.java