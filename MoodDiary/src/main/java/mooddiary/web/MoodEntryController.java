package mooddiary.web;

import mooddiary.domain.MoodEntry;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/moodentrys")
@Controller
@RooWebScaffold(path = "moodentrys", formBackingObject = MoodEntry.class, exposeFinders=false, exposeJson=false)
public class MoodEntryController {
}
