package mooddiary.web;

import mooddiary.domain.Suggestion;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/suggestions")
@Controller
@RooWebScaffold(path = "suggestions", formBackingObject = Suggestion.class, delete=false, update=false, create=false, exposeFinders=false, exposeJson=false)
public class SuggestionController {
}
