// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package mooddiary.domain;

import java.util.List;
import mooddiary.domain.Suggestion;
import mooddiary.domain.SuggestionDataOnDemand;
import mooddiary.domain.SuggestionIntegrationTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect SuggestionIntegrationTest_Roo_IntegrationTest {
    
    declare @type: SuggestionIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: SuggestionIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml");
    
    declare @type: SuggestionIntegrationTest: @Transactional;
    
    @Autowired
    private SuggestionDataOnDemand SuggestionIntegrationTest.dod;
    
    @Test
    public void SuggestionIntegrationTest.testCountSuggestions() {
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", dod.getRandomSuggestion());
        long count = Suggestion.countSuggestions();
        Assert.assertTrue("Counter for 'Suggestion' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void SuggestionIntegrationTest.testFindSuggestion() {
        Suggestion obj = dod.getRandomSuggestion();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to provide an identifier", id);
        obj = Suggestion.findSuggestion(id);
        Assert.assertNotNull("Find method for 'Suggestion' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'Suggestion' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void SuggestionIntegrationTest.testFindAllSuggestions() {
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", dod.getRandomSuggestion());
        long count = Suggestion.countSuggestions();
        Assert.assertTrue("Too expensive to perform a find all test for 'Suggestion', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<Suggestion> result = Suggestion.findAllSuggestions();
        Assert.assertNotNull("Find all method for 'Suggestion' illegally returned null", result);
        Assert.assertTrue("Find all method for 'Suggestion' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void SuggestionIntegrationTest.testFindSuggestionEntries() {
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", dod.getRandomSuggestion());
        long count = Suggestion.countSuggestions();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<Suggestion> result = Suggestion.findSuggestionEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'Suggestion' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'Suggestion' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void SuggestionIntegrationTest.testFlush() {
        Suggestion obj = dod.getRandomSuggestion();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to provide an identifier", id);
        obj = Suggestion.findSuggestion(id);
        Assert.assertNotNull("Find method for 'Suggestion' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifySuggestion(obj);
        Integer currentVersion = obj.getVersion();
        obj.flush();
        Assert.assertTrue("Version for 'Suggestion' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void SuggestionIntegrationTest.testMergeUpdate() {
        Suggestion obj = dod.getRandomSuggestion();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to provide an identifier", id);
        obj = Suggestion.findSuggestion(id);
        boolean modified =  dod.modifySuggestion(obj);
        Integer currentVersion = obj.getVersion();
        Suggestion merged = obj.merge();
        obj.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'Suggestion' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void SuggestionIntegrationTest.testPersist() {
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", dod.getRandomSuggestion());
        Suggestion obj = dod.getNewTransientSuggestion(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'Suggestion' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        Assert.assertNotNull("Expected 'Suggestion' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void SuggestionIntegrationTest.testRemove() {
        Suggestion obj = dod.getRandomSuggestion();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Suggestion' failed to provide an identifier", id);
        obj = Suggestion.findSuggestion(id);
        obj.remove();
        obj.flush();
        Assert.assertNull("Failed to remove 'Suggestion' with identifier '" + id + "'", Suggestion.findSuggestion(id));
    }
    
}
