package mooddiary.domain;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = MoodEntry.class)
public class MoodEntryDataOnDemand {
}
